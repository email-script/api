require("dotenv").config();

const fastify = require("fastify")({
  logger: true,
});
const cors = require("fastify-cors");

const sites = require("./services/sites");
const view = require("./services/template");
const emailAlerts = require("./services/email-alerts");

fastify.register(cors, {
  origin: process.env.CORS_ORIGIN || "*",
});

// Services
fastify.get("/sites", sites.get);
fastify.get("/template", view.get);
fastify.post("/email-alerts", emailAlerts.post);

(async () => {
  try {
    const port = process.env.PORT || 3080;
    const address = await fastify.listen(port, "0.0.0.0");
    fastify.log.info(`server listening on ${address}`);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
})();
