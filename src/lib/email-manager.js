const { nunjucks } = require("./template-engine");

module.exports = {
  notifyFailed(failedSites) {
    let failureTemplate = nunjucks.render("failure.mjml", {
      failedSites,
    });
    console.log(`Failed sites: `, failedSites);
    // console.log(failureTemplate);
  },
};
