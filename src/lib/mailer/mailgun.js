const FormData = require("form-data");
const { objectToFormData } = require("object-to-formdata");
const got = require("got");

const baseUrl = `https://api.mailgun.net/v3/${process.env.DOMAIN}`;

const getServiceEndpoint = (service) => baseUrl.concat(service);

const mailgun = {
  messages: {
    send(data) {
      const endpoint = getServiceEndpoint("/messages");

      // Convert data into form data
      const body = objectToFormData(data, {}, new FormData());

      return got.post(endpoint, {
        body,
        username: "api",
        password: process.env.API_KEY,
      });
    },
  },
};

module.exports = mailgun;
