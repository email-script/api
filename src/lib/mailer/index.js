const mjml2html = require("mjml");
const { messages } = require("./mailgun");

const send = async (template, type) => {
  // Construct the email message
  const data = {
    from: process.env.EMAIL_FROM,
    to: "PUTYOUREMAIL@EMAIL.COM",
    subject: `Maintenance [${type}]`,
    html: mjml2html(template).html,
  };
  return await messages.send(data);
};

module.exports = {
  send,
};
