const sites = require("../data/sites.json");

const statuses = sites.reduce(
  (collector, site) => ({
    ...collector,
    [site]: true,
  }),
  {}
);

module.exports = {
  statuses,
  sites,
};
