const nunjucks = require("nunjucks");
const path = require("path");

const viewPath = path.resolve(__dirname, "../views");
nunjucks.configure(viewPath, { autoescape: true });

module.exports = {
  nunjucks,
};
