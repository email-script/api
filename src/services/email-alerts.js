const { nunjucks } = require("../lib/template-engine");
const { send } = require("../lib/mailer");

const templateList = {
  resumed: "resumed",
  planned: "planned-maintenance",
  unplanned: "unplanned-maintenance",
};

module.exports = {
  async post(request, reply) {
    const emailAlert = request.body;
    const templateName = templateList[emailAlert.type];

    alert = nunjucks.render(`${templateName}.mjml`, {
      details: emailAlert.details,
      sites: emailAlert.sites,
    });

    try {
      return await send(alert, emailAlert.type);
    } catch (error) {
      return error;
    }
  },
};
