const got = require("got");
const mailer = require("../lib/email-manager.js");
const { statuses, sites } = require("../lib/db.js");

// in milliseconds
const interval = 15e3;
const timeout = 8e3;

setInterval(() => {
  const siteChecks = sites.map(async (site) => {
    let status;

    try {
      const response = await got(site, {
        timeout,
      });

      console.info(`${site} is up!`);
      console.debug(
        `${site} took ${response.timings.phases.total} milliseconds to resolve`
      );

      status = response.statusCode <= 400;
    } catch (error) {
      console.info(`${site} is down!`);
      console.error(String(error));
      status = false;
    }

    return {
      site,
      status,
    };
  });

  Promise.all(siteChecks).then((results) => {
    results.forEach(({ site, status }) => {
      statuses[site] = status;
    });

    // test for any failures and send a notification if any were found
    const failedSites = results
      .filter(({ status }) => !status)
      .map(({ site }) => site);

    if (failedSites.length) {
      mailer.notifyFailed(failedSites);
    }

    // don't spam
  });
}, interval);

module.exports = {
  async get(request, reply) {
    return statuses;
  },
};
