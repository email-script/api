const { nunjucks } = require("../lib/template-engine.js");

module.exports = {
  async get(request, reply) {
    let renderedTemplate = "nothing!";

    try {
      renderedTemplate = nunjucks.render("failure.mjml", { foo: "bar" });
    } catch (error) {
      console.error(String(error));
    }

    return renderedTemplate;
  },
};
